#!/bin/sh

set -e

readonly version="0.36.0"
readonly sha256sum="100ad4bd66cda0d50ee34745f75677db0ed040a8ce93a02e8e63421635bd102d"
readonly filename="cargo-semver-checks-x86_64-unknown-linux-musl.tar.gz"

cd .gitlab

echo "$sha256sum  $filename" > cargo-semver-checks.sha256sum
curl -OL "https://github.com/obi1kenobi/cargo-semver-checks/releases/download/v$version/$filename"
sha256sum --check cargo-semver-checks.sha256sum
tar -xf "$filename"
