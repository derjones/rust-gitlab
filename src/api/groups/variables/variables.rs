// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use derive_builder::Builder;

use crate::api::common::NameOrId;
use crate::api::endpoint_prelude::*;

/// Query for list of variables
#[derive(Debug, Builder, Clone)]
#[builder(setter(strip_option))]
pub struct GroupVariables<'a> {
    /// The group to query variables from
    #[builder(setter(into))]
    group: NameOrId<'a>,
}

impl<'a> GroupVariables<'a> {
    /// Create a builder for the endpoint.
    pub fn builder() -> GroupVariablesBuilder<'a> {
        GroupVariablesBuilder::default()
    }
}

impl Endpoint for GroupVariables<'_> {
    fn method(&self) -> Method {
        Method::GET
    }

    fn endpoint(&self) -> Cow<'static, str> {
        format!("groups/{}/variables", self.group).into()
    }
}

impl Pageable for GroupVariables<'_> {}

#[cfg(test)]
mod tests {
    use http::Method;

    use crate::api::groups::variables::variables::{GroupVariables, GroupVariablesBuilderError};
    use crate::api::{self, Query};
    use crate::test::client::{ExpectedUrl, SingleTestClient};

    #[test]
    fn all_parameters_are_needed() {
        let err = GroupVariables::builder().build().unwrap_err();
        crate::test::assert_missing_field!(err, GroupVariablesBuilderError, "group");
    }

    #[test]
    fn sufficient_parameters() {
        GroupVariables::builder().group(1).build().unwrap();
    }

    #[test]
    fn endpoint() {
        let endpoint = ExpectedUrl::builder()
            .method(Method::GET)
            .endpoint("groups/simple%2Fgroup/variables")
            .build()
            .unwrap();
        let client = SingleTestClient::new_raw(endpoint, "");

        let endpoint = GroupVariables::builder()
            .group("simple/group")
            .build()
            .unwrap();
        api::ignore(endpoint).query(&client).unwrap();
    }
}
