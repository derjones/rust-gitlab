// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use derive_builder::Builder;

use crate::api::common::{self, NameOrId};
use crate::api::endpoint_prelude::*;
use crate::api::projects::variables::ProjectVariableFilter;

/// Delete a variable of a project.
#[derive(Debug, Builder, Clone)]
#[builder(setter(strip_option))]
pub struct DeleteProjectVariable<'a> {
    /// The project to remove the variable from.
    #[builder(setter(into))]
    project: NameOrId<'a>,
    /// The name of the variable.
    #[builder(setter(into))]
    key: Cow<'a, str>,
    /// The environment scope filter of the variable.
    #[builder(default)]
    filter: Option<ProjectVariableFilter<'a>>,
}

impl<'a> DeleteProjectVariable<'a> {
    /// Create a builder for the endpoint.
    pub fn builder() -> DeleteProjectVariableBuilder<'a> {
        DeleteProjectVariableBuilder::default()
    }
}

impl Endpoint for DeleteProjectVariable<'_> {
    fn method(&self) -> Method {
        Method::DELETE
    }

    fn endpoint(&self) -> Cow<'static, str> {
        format!(
            "projects/{}/variables/{}",
            self.project,
            common::path_escaped(&self.key),
        )
        .into()
    }

    fn body(&self) -> Result<Option<(&'static str, Vec<u8>)>, BodyError> {
        let mut params = FormParams::default();

        if let Some(filter) = self.filter.as_ref() {
            filter.add_query(&mut params);
        }

        params.into_body()
    }
}

#[cfg(test)]
mod tests {
    use http::Method;

    use crate::api::projects::variables::delete::{
        DeleteProjectVariable, DeleteProjectVariableBuilderError, ProjectVariableFilter,
    };
    use crate::api::{self, Query};
    use crate::test::client::{ExpectedUrl, SingleTestClient};

    #[test]
    fn all_parameters_are_needed() {
        let err = DeleteProjectVariable::builder().build().unwrap_err();
        crate::test::assert_missing_field!(err, DeleteProjectVariableBuilderError, "project");
    }

    #[test]
    fn project_is_necessary() {
        let err = DeleteProjectVariable::builder()
            .key("testkey")
            .build()
            .unwrap_err();
        crate::test::assert_missing_field!(err, DeleteProjectVariableBuilderError, "project");
    }

    #[test]
    fn key_is_necessary() {
        let err = DeleteProjectVariable::builder()
            .project(1)
            .build()
            .unwrap_err();
        crate::test::assert_missing_field!(err, DeleteProjectVariableBuilderError, "key");
    }

    #[test]
    fn sufficient_parameters() {
        DeleteProjectVariable::builder()
            .project(1)
            .key("testkey")
            .build()
            .unwrap();
    }

    #[test]
    fn endpoint() {
        let endpoint = ExpectedUrl::builder()
            .method(Method::DELETE)
            .endpoint("projects/simple%2Fproject/variables/testkey")
            .content_type("application/x-www-form-urlencoded")
            .build()
            .unwrap();
        let client = SingleTestClient::new_raw(endpoint, "");

        let endpoint = DeleteProjectVariable::builder()
            .project("simple/project")
            .key("testkey")
            .build()
            .unwrap();
        api::ignore(endpoint).query(&client).unwrap();
    }

    #[test]
    fn endpoint_filter() {
        let endpoint = ExpectedUrl::builder()
            .method(Method::DELETE)
            .endpoint("projects/simple%2Fproject/variables/testkey")
            .content_type("application/x-www-form-urlencoded")
            .body_str("filter%5Benvironment_scope%5D=production")
            .build()
            .unwrap();
        let client = SingleTestClient::new_raw(endpoint, "");

        let endpoint = DeleteProjectVariable::builder()
            .project("simple/project")
            .key("testkey")
            .filter(
                ProjectVariableFilter::builder()
                    .environment_scope("production")
                    .build()
                    .unwrap(),
            )
            .build()
            .unwrap();
        api::ignore(endpoint).query(&client).unwrap();
    }
}
