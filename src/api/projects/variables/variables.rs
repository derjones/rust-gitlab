// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use derive_builder::Builder;

use crate::api::common::NameOrId;
use crate::api::endpoint_prelude::*;

/// Query for list of variables
#[derive(Debug, Builder, Clone)]
#[builder(setter(strip_option))]
pub struct ProjectVariables<'a> {
    /// The project to query variables from
    #[builder(setter(into))]
    project: NameOrId<'a>,
}

impl<'a> ProjectVariables<'a> {
    /// Create a builder for the endpoint.
    pub fn builder() -> ProjectVariablesBuilder<'a> {
        ProjectVariablesBuilder::default()
    }
}

impl Endpoint for ProjectVariables<'_> {
    fn method(&self) -> Method {
        Method::GET
    }

    fn endpoint(&self) -> Cow<'static, str> {
        format!("projects/{}/variables", self.project).into()
    }
}

impl Pageable for ProjectVariables<'_> {}

#[cfg(test)]
mod tests {
    use http::Method;

    use crate::api::projects::variables::variables::{
        ProjectVariables, ProjectVariablesBuilderError,
    };
    use crate::api::{self, Query};
    use crate::test::client::{ExpectedUrl, SingleTestClient};

    #[test]
    fn all_parameters_are_needed() {
        let err = ProjectVariables::builder().build().unwrap_err();
        crate::test::assert_missing_field!(err, ProjectVariablesBuilderError, "project");
    }

    #[test]
    fn sufficient_parameters() {
        ProjectVariables::builder().project(1).build().unwrap();
    }

    #[test]
    fn endpoint() {
        let endpoint = ExpectedUrl::builder()
            .method(Method::GET)
            .endpoint("projects/simple%2Fproject/variables")
            .build()
            .unwrap();
        let client = SingleTestClient::new_raw(endpoint, "");

        let endpoint = ProjectVariables::builder()
            .project("simple/project")
            .build()
            .unwrap();
        api::ignore(endpoint).query(&client).unwrap();
    }
}
